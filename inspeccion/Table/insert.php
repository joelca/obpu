<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>

<?php

if (!isset($_POST['submit'])) {
// Formulario no enviado
?>

    <form action="" method="post">
    Color: <input type="text" name="color">
    Fruta: <input type="text" name="fruta">
    <input type="submit" name="submit">
    </form>

<?php
}
else {
// Formulario enviado

    include("includes/conexion.php");

    // Verifico los valores enviados y los ajusto por seguridad
    $color = empty($_POST['color']) ? die ("ERROR: Ingrese color") : $_POST['color']; // deberóa tener que usar mysql_escape_string u otra función
    $fruta = empty($_POST['fruta']) ? die ("ERROR: Ingrese fruta") : $_POST['fruta'];

    // abrir conexión
    $connection = mysqli_connect($host, $user, $pass) or die ("Error en la conexión");

    // seleccionar DB
    mysqli_select_db($connection,$db) or die ("Error al seleccionar DB!");

    // creo la consulta
    $query = "INSERT INTO frutas (color, fruta) VALUES ('$color', '$fruta')";

    // ejecuto la consulta
    $result = mysqli_query($connection,$query) or die ("Error en la consulta: $query. ".mysqli_error($connection));

    // muestro el valor de la clave primaria que se ha creado
    echo "Nuevo registro insertado con ID: ".mysqli_insert_id($connection);

	// no necesito liberar la memoria porque el INSERT no devuelve un conjunto de datos.
}
?>

<p><a href="index.php">Volver</a></p>

</body>
</html>