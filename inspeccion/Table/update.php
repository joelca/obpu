<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php

include("includes/conexion.php");

if (!isset($_POST['submit'])){
	// Formulario no enviado
	if (isset($_GET['id'])) {
	//recibiendo un parámetro por el URL, para saber qué registro editar

		// abrir conexión
		$connection = mysqli_connect($host, $user, $pass) or die ("Error en la conexión");

		// seleccionar DB
		mysqli_select_db($connection,$db) or die ("Error al seleccionar DB!");

		// creo la consulta
		$query = "SELECT * FROM frutas WHERE id=".$_GET['id'];

		// ejecuto la consulta
		$result = mysqli_query($connection,$query) or die ("Error en la consulta: $query. ".mysqli_error($connection));

		// verifico si hay datos devueltos
		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_assoc($result)
?>
                <form action="" method="post">
                  Color:
                  <input name="color" type="text" value="<?php echo $row['color'] ?>">
                  Fruta:
                  <input type="text" name="fruta"  value="<?php echo $row['fruta'] ?>">
                  <input name="id" type="hidden" id="id" value="<?php echo $row['id'] ?>">
                  <input type="submit" name="submit">
                </form>
<?php
		} else {
			// no
			echo "El registro no existe!";
		}

		// Libero la memoria
		mysqli_free_result($result);
	}
} else {
// Formulario enviado

    // Verifico los valores enviados y los ajusto por seguridad
    $color = empty($_POST['color']) ? die ("ERROR: Ingrese color") : $_POST['color']; //podría tener que usar mysql_escape_string u otra función
    $fruta = empty($_POST['fruta']) ? die ("ERROR: Ingrese fruta") : $_POST['fruta'];
	//Recupero la clave del registro que debo actualizar
	$id = $_POST['id'];

    // abrir conexión
    $connection = mysqli_connect($host, $user, $pass) or die ("Error en la conexión");

    // seleccionar DB
    mysqli_select_db($connection,$db) or die ("Error al seleccionar DB!");

    // creo la consulta
    $query = "UPDATE frutas SET color='$color', fruta='$fruta' WHERE id=$id";

    // ejecuto la consulta
    $result = mysqli_query($connection,$query) or die ("Error en la consulta: $query. ".mysqli_error($connection));

	echo "Datos actualizados";

	// no necesito liberar la memoria porque el UPDATE no devuelve un conjunto de datos.

}
?>

<p><a href="index.php">Volver</a></p>

</body>
</html>
