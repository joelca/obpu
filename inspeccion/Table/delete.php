<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php

if (!isset($_POST['submit'])){
	// Formulario no enviado
	if (isset($_GET['id'])) {
	//recibiendo un parámetro por el URL, para saber qué registro eliminar

		include("includes/conexion.php");

		// abrir conexión
		$connection = mysqli_connect($host, $user, $pass) or die ("Error en la conexión");

		// seleccionar DB
		mysqli_select_db($connection,$db) or die ("Error al seleccionar DB!");

		// creo la consulta
		$query = "SELECT * FROM frutas WHERE id=".$_GET['id'];

		// ejecuto la consulta
		$result = mysqli_query($connection,$query) or die ("Error en la consulta: $query. ".mysqli_error($connection));

		// verifico si hay datos devueltos
		if (mysqli_num_rows($result) > 0) {
			$row = mysqli_fetch_assoc($result)
?>
                <form action="" method="post">
                <p> ¿Desea eliminar el registro <?php echo $row['fruta'] ?> <?php echo $row['color'] ?>?</p>
                  <input name="id" type="hidden" id="id" value="<?php echo $row['id'] ?>">
                  <input type="submit" name="submit">
                </form>
<?php
		} else {
			// no
			echo "El registro no existe!";
		}


		// Libero la memoria
		mysqli_free_result($result);
	}
} else {
// Formulario enviado

    include("includes/conexion.php");

	//Recupero la clave del registro que debo eliminar
	$id = $_POST['id'];

    // abrir conexión
    $connection = mysqli_connect($host, $user, $pass) or die ("Error en la conexión");

    // seleccionar DB
    mysqli_select_db($connection,$db) or die ("Error al seleccionar DB!");

    // creo la consulta
     $query = "DELETE FROM frutas WHERE id = ".$_POST['id'];

    // ejecuto la consulta
    $result = mysqli_query($connection,$query) or die ("Error en la consulta: $query. ".mysqli_error($connection));

    unset($_POST);

	//muestro la cantidad de registros afectados por la consulta
	echo mysqli_affected_rows($connection)." registros afectados";

	// no necesito liberar la memoria porque el DELETE no devuelve un conjunto de datos.

}

?>

<p><a href="index.php">Volver</a></p>

</body>
</html>
