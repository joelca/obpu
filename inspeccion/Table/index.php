<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>

<a href="insert.php">Insertar</a>

<?php

include("includes/conexion.php");

// abrir conexión
$connection = mysqli_connect($host, $user, $pass) or die ("Error en la conexión");

// seleccionar DB
mysqli_select_db($connection,$db) or die ("Error al seleccionar DB!");

// creo la consulta
$query = "SELECT * FROM frutas";

// ejecuto la consulta
$result = mysqli_query($connection,$query) or die ("Error en la consulta: $query. ".mysqli_error($connection));

// verifico si hay datos devueltos
if (mysqli_num_rows($result) > 0) {
    // si
?>

<table>

<?php
    while($row = mysqli_fetch_assoc($result)) {  //Devuelve los registros de a uno en un arreglo asociativo, y falso al final.
?>
    <tr>
        <td><?php echo $row['id'] ?></td>
        <td><?php echo $row['color'] ?></td>
        <td><?php echo $row['fruta'] ?></td>
        <td><a href="update.php?id=<?php echo $row['id'] ?>">Editar</a></td>
        <td><a href="delete.php?id=<?php echo $row['id'] ?>">Borrar</a></td>
    </tr>
<?php
    }
?>
</table>

<?php
} else {
    // no
    echo "No hay registros!";
}

// Libero la memoria
mysqli_free_result($result);

?>

</body>
</html>