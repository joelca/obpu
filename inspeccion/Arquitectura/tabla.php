<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Base de datos</title>
  <style>
    body{ padding: 1% 3%; color: rgb(119, 119, 119); }
    h1{ color:#333 }
  </style>

  <!-- Agregamos algo de estilo a la pagina -->
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
</head>

<body>
<h1>Editable table example</h1>

<?php
	function table($result) {
    	$result->fetch_array(MYSQLI_ASSOC);
    	echo '<table id="editable" class="pure-table pure-table-bordered">';
   		tableHead( $result );
    	tableBody( $result );
    	echo '</table>';
	}

	function tableHead($result) {
    	echo '<thead>';
    	foreach ($result as $x) {
   		echo '<tr>';
    	foreach ($x as $k => $y) {
        	echo '<th>' . ucfirst($k) . '</th>';
    	}
    	echo '</tr>';
    	break;
    	}
    	echo '</thead>';
	}

	function tableBody($result) {
    	echo '<tbody>';
    	foreach ($result as $x) {
    	echo '<tr>';
    	foreach ($x as $y) {
        	echo '<td>' . $y . '</td>';
    	}
    	echo '</tr>';
    	}
    	echo '</tbody>';
	}

	// Crear conexion
	$connect = mysqli_connect("localhost", "root", "", "login") or die ("Error en la conexión");

	//Ejecutar la consulta
	$query = mysqli_query($connect, "SELECT * FROM usuarios");

	table($query);
?>

<!-- Link a jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script> 
<!-- Link a libreria para poder editar celdas -->
<script src="mindmup-editabletable.js"></script>

<script>
	$('#editable').editableTableWidget();
</script>

</body>
</html>